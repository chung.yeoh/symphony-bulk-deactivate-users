import logging, csv
from sym_api_client_python.configure.configure import SymConfig
from sym_api_client_python.auth.rsa_auth import SymBotRSAAuth
from sym_api_client_python.clients.sym_bot_client import SymBotClient
from sym_api_client_python.clients.admin_client import AdminClient

# Input/Output File Names
INPUT_FILE = 'symphony_bulk_deactivate.csv'
OUTPUT_FILE = 'symphony_bulk_deactivate_output.csv'



def configure_logging():
    logging.basicConfig(
            filename='./logs/output.log',
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            filemode='w', level=logging.DEBUG
    )
    logging.getLogger("urllib3").setLevel(logging.WARNING)


def main():
    print('Python Client runs using RSA authentication')

    # Configure log
    configure_logging()

    # RSA Auth flow: pass path to rsa config.json file
    configure = SymConfig('./resources/config.json')
    configure.load_config()
    auth = SymBotRSAAuth(configure)
    print('Start Authenticating..')
    logging.info('Start Authenticating..')
    auth.authenticate()

    # Initialize SymBotClient with auth and configure objects
    bot_client = SymBotClient(auth, configure)
    admin_client = AdminClient(bot_client)

    # Retrieve list of users
    print('Retrieve All Active Pod Users...')
    logging.info('Retrieve All Active Pod Users...')

    email_dict, username_dict = retrieve_all_active_pod_users(admin_client)

    print(f'Retrieved {str(len(username_dict))} users')
    logging.info(f'Retrieved {str(len(username_dict))} users')

    # Now process CSV file
    # CSV file will have 2 columns - Username, Email)
    process_result = []
    with open(INPUT_FILE, newline='') as csvfile:
        csv_list = csv.reader(csvfile, delimiter=',')
        for row in csv_list:
            # Ignore blank rows
            if len(row) == 0:
                continue

            # Ensure CSV has 2 columns
            if len(row) < 1:
                logging.error('Invalid CSV File Format - Expect 1 columns - UserName/Email')
                raise Exception('Invalid CSV File Format - Expect 1 columns - UserName/Email')

            # Skip header row
            if row[0] == "UserName/Email":
                continue

            result_record = dict()
            # Get row values
            result_record['result'] = ''
            result_record['user_id'] = 0
            result_record['username_email'] = row[0]

            # Check if user exist
            if result_record['username_email'] in email_dict:
                result_record['user_id'] = email_dict[result_record['username_email']]
            elif result_record['username_email'] in username_dict:
                result_record['user_id'] = username_dict[result_record['username_email']]
            else:
                print(f"{result_record['username_email']} is Inactive or not found - Skip record")
                logging.warning(f"{result_record['username_email']} is Inactive or not found - Skip record")
                result_record['result'] = 'UserName/Email not found - SKIPPED'
                process_result.append(result_record)
                continue


            # Deactivate User
            print(f"Deactivating User - UserID: {result_record['user_id']} / UserName or Email: {result_record['username_email']}")
            admin_client.admin_update_user_status(result_record['user_id'], 'DISABLED')

            # Everything done successfully
            result_record['result'] = 'OK'
            process_result.append(result_record)


    # Print final result
    print(f'Generating Result Files...')
    logging.info(f'Generating Result Files...')
    print_result(process_result)


def print_result(process_result):
    with open(OUTPUT_FILE, 'w', newline='', encoding='utf-8-sig') as csvfile:
        fieldnames = ['UserName/Email',
                      'Status']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for row in process_result:
            writer.writerow(
                {'UserName/Email': row['username_email'],
                 'Status': row['result']})

    return


def retrieve_all_active_pod_users(admin_client):
    output = admin_client.admin_list_users(skip=0, limit=1000)

    while True:
        next_out = admin_client.admin_list_users(skip=int(len(output)), limit=1000)
        if len(next_out) > 0:
            for index in range(len(next_out)): output.append(next_out[index])
        else:
            break

    # Filter out Disabled users and create Dictionary
    email_dict = dict()
    username_dict = dict()
    for u in output:
        if u["userSystemInfo"]["status"] == "ENABLED":
            email = u['userAttributes']['emailAddress']
            username = u['userAttributes']['userName']
            user_id = u['userSystemInfo']['id']

            email_dict[email] = user_id
            username_dict[username] = user_id

    return email_dict, username_dict


if __name__ == "__main__":
    main()