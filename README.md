# Symphony Bulk Deactivate Users

## Overview
This Python script is built based on the [Symphony Python SDK](https://github.com/SymphonyPlatformSolutions/symphony-api-client-python)

The script will be able to perform the following:
- Deactivate users based on given input CSV file

**Note** - you may mix different Username or Email address in the CSV file

The script expects a CSV file as input.
Upon completion, the script will produce an CSV file as output containing the results.

The input and output file names can be adjusted at the top of the script

    # Input/Output File Names
    INPUT_FILE = 'symphony_bulk_deactivate.csv'
    OUTPUT_FILE = 'symphony_bulk_deactivate_output.csv'

## Input CSV Columns
The script expects an input CSV file at the top directory where the script runs with filename - ``symphony_bulk_deactivate.csv``

The CSV file will contain following columns:
- UserName/Email (User Name or Email Address of User to be deactivated)

A few things
to take note of:
- If Username or Email is not found in the pod, it will be skipped and ignored
- If user is already deactivated, it will also be skipped and ignored

## Output CSV Columns
The output file will be saved in the same directory as the input file with filename - ``symphony_bulk_deactivate_output.csv``

Columns will be same as Input CSV above, with additional of **Status** column.

Successful entries will be marked with status = OK. Otherwise, error will be displayed


## Environment Setup
This client is compatible with **Python 3.6 or above**

Create a virtual environment by executing the following command **(optional)**:
``python3 -m venv ./venv``

Activate the virtual environment **(optional)**:
``source ./venv/bin/activate``

Install dependencies required for this client by executing the command below.
``pip install -r requirements.txt``


## Getting Started
### 1 - Prepare the service account
The Python client operates using a [Symphony Service Account](https://support.symphony.com/hc/en-us/articles/360000720863-Create-a-new-service-account), which is a type of account that applications use to work with Symphony APIs. Please contact with Symphony Admin in your company to get the account.

The client currently supports two types of Service Account authentications, they are
[Client Certificates Authentication](https://symphony-developers.symphony.com/symphony-developer/docs/bot-authentication-workflow#section-authentication-using-client-certificates)
and [RSA Public/Private Key Pair Authentication](https://symphony-developers.symphony.com/symphony-developer/docs/rsa-bot-authentication-workflow).

**RSA Public/Private Key Pair** is the recommended authentication mechanism by Symphony, due to its robust security and simplicity.

**Important** - The service account must have **User Provisioning** role in order to work.

### 2 - Upload Service Account Private Key
Please copy the Service Account private key file (*.pem) to the **rsa** folder. You will need to configure this in the next step.

### 3 - Update resources/config.json

To run the bot, you will need to configure **config.json** provided in the **resources** directory. 

**Notes:**
Most of the time, the **port numbers** do not need to be changed.

You should replace **pod** with your actual Pod URL.

You also need to update based on the service account created above:
- botPrivateKeyPath (ends with a trailing "/"))
- botPrivateKeyName
- botUsername
- botEmailAddress

Sample:

    {
      "sessionAuthHost": "<pod>.symphony.com",
      "sessionAuthPort": 443,
      "keyAuthHost": "<pod>.symphony.com",
      "keyAuthPort": 443,
      "podHost": "<pod>.symphony.com",
      "podPort": 443,
      "agentHost": "<pod>.symphony.com",
      "agentPort": 443,
      "authType": "rsa",
      "botPrivateKeyPath":"./rsa/",
      "botPrivateKeyName": "bot-private-key.pem",
      "botCertPath": "",
      "botCertName": "",
      "botCertPassword": "",
      "botUsername": "<bot-user>",
      "botEmailAddress": "<bot-email>",
      "appCertPath": "",
      "appCertName": "",
      "appCertPassword": "",
      "authTokenRefreshPeriod": "30",
      "proxyURL": "",
      "proxyUsername": "",
      "proxyPassword": "",
      "podProxyURL": "",
      "podProxyUsername": "",
      "podProxyPassword": "",
      "agentProxyURL": "",
      "agentProxyUsername": "",
      "agentProxyPassword": "",
      "keyManagerProxyURL": "",
      "keyManagerProxyUsername": "",
      "keyManagerProxyPassword": "",
      "truststorePath": ""
    }

### 4 - Run script
The script can be executed by running
``python3 bulk_user_deactivate.py`` 



# Release Notes

## 0.1
- Initial Release

